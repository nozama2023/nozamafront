export interface User {
    id: number;
    lastname: string;
    firstname: string;
    gender: string;
    mobile: number;
    mail: string;
    birthdate: string;
    password: string;
    signInDate: number;

}