export interface Product {
    id: number;
    reference: string;
    name: string;
    description: string;
    quantityStock: number;
    price: number;
    category: string;
    imageUrl: string;
}