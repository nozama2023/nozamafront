export interface Adress {
    id: number;
    firstname: string;
    name: string;
    mobile: number;
    adress: string;
    adressCode: number;
    town: string;
}