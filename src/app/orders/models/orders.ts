export interface Orders{
    id: number,
    idUser: number,
    status: string,
    date: string,
    price: number,
}